<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;

class TripayController extends Controller
{
    public function getPaymentChannels()
    {


        $apiKey = config('tripay.api_key'); //<--- folder config - file tripay.php - api_key



        // $payload = [
        //     'code'    => 'BRIVA'
        // ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_FRESH_CONNECT     => true,
            CURLOPT_URL               => "https://tripay.co.id/api-sandbox/merchant/payment-channel?",
            CURLOPT_RETURNTRANSFER    => true,
            CURLOPT_HEADER            => false,
            CURLOPT_HTTPHEADER        => array(
                "Authorization: Bearer " . $apiKey
            ),
            CURLOPT_FAILONERROR       => false
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response)->data;

        // dd(json_decode($response)->data);
        return $response ? $response : $err;
    }

    public function requestTransaction($method, $book)
    {


        $apiKey = config('tripay.api_key');
        $privateKey = config('tripay.private_key');
        $merchantCode = config('tripay.merchant_code');

        // dd($apiKey, $privateKey, $merchantCode);

        $merchantRef = 'PX-' . time();
        $users = auth()->user();


        $data = [
            'method'            => $method,
            'merchant_ref'      => $merchantRef,
            'amount'            => $book->price,
            'customer_name'     => $users->name,
            'customer_email'    => $users->email,
            // 'customer_phone'    => '081234567890',
            'order_items'       => [
                [

                    'name'      => $book->title,
                    'price'     => $book->price,
                    'quantity'  => 1
                ]
            ],

            'expired_time'      => (time() + (24 * 60 * 60)), // 24 jam
            'signature'         => hash_hmac('sha256', $merchantCode . $merchantRef . $book->price, $privateKey)
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_FRESH_CONNECT     => true,
            CURLOPT_URL               => "https://tripay.co.id/api-sandbox/transaction/create",
            CURLOPT_RETURNTRANSFER    => true,
            CURLOPT_HEADER            => false,
            CURLOPT_HTTPHEADER        => array(
                "Authorization: Bearer " . $apiKey
            ),
            CURLOPT_FAILONERROR       => false,
            CURLOPT_POST              => true,
            CURLOPT_POSTFIELDS        => http_build_query($data)
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response)->data;
        // dd($response);

        return $response ? $response : $err;
    }

    public function detailTransaction($reference)
    {


        $apiKey = config('tripay.api_key');

        $payload = [
            'reference'    => $reference,
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_FRESH_CONNECT     => true,
            CURLOPT_URL               => "https://tripay.co.id/api-sandbox/transaction/detail?" . http_build_query($payload),
            CURLOPT_RETURNTRANSFER    => true,
            CURLOPT_HEADER            => false,
            CURLOPT_HTTPHEADER        => array(
                "Authorization: Bearer " . $apiKey
            ),
            CURLOPT_FAILONERROR       => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response)->data;


        return $response ? $response : $err;
    }
}
